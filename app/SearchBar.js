import React, { Component } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

{/* Class for the search bar component */}
export default class SearchBar extends Component {

    render() {
        {/* Render view of the search field */}
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    placeholder="Search"
                    onChangeText={this.props.onChange}
                />
            </View>
        )
    }
}

{/* Style sheet */}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 8,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#C9C9CE',
    },
    input: {
        height: 30,
        flex: 1,
        paddingHorizontal: 8,
        backgroundColor: '#FFFFFF',
        borderRadius: 4,
    },
});

