{/* Base url of the server */}
const baseURL = 'http://172.16.200.34:5000/employees';

{/* Api to fetch the records of employees */}
export let findAll = () => fetch(baseURL)
    .then((response) => response.json());

{/* Api to fetch the records of employees according to name */}
export let findByName = (name) => fetch(`${baseURL}?name=${name}`)
    .then((response) => response.json());

{/* Api to fetch the records of employees according to the Id */}
export let findById = (id) => fetch(`${baseURL}/${id}`)
    .then((response) => response.json());